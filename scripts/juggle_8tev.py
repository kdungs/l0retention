"""
This script process a DST with the RawEvent split up and writes out the
DAQ/RawEvent to the output DST. The test should check for the existence of
DAQ/RawEvent in the TES of reocmbined.dst
"""

from glob import glob
from Gaudi.Configuration import *
from GaudiConf import IOHelper
from LHCbKernel.Configuration import *
from Configurables import (
    GaudiSequencer,
    LHCbApp,
    RawEventJuggler
)

TCK = '0x0094003D'
FOLDER = 'files/8'

LHCbApp()
MySeq = GaudiSequencer('MoveTCKtoNewLocation')
MyWriter = InputCopyStream('CopyToFile')
RawEventJuggler().TCK = TCK
RawEventJuggler().Input = 'Stripping20'
RawEventJuggler().Output = 0.0
RawEventJuggler().Sequencer = MySeq
RawEventJuggler().WriterOptItemList = MyWriter
RawEventJuggler().KillExtraBanks = True
RawEventJuggler().KillExtraNodes = True
ApplicationMgr().TopAlg = [MySeq]
LHCbApp().EvtMax = 20000
IOHelper().inputFiles(glob('{0}/*.dst'.format(FOLDER)))
IOHelper().outStream('{}/juggled.dst'.format(FOLDER), MyWriter)
