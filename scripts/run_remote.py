#!/usr/bin/env python

MAX_NODES = 100
SCRIPT = '/home/kdungs/data/l0retention/scripts/get_files_for_run.py'


def hostname(farm, subfarm, node):
    return 'hlt{0}{1:02}{2:02}'.format(farm, subfarm, node)


def nodes():
    import itertools as it
    return (hostname(*n) for n in it.product(
        ('a', 'b', 'c', 'd', 'e'),
        range(1, 10),
        range(1, 30)
    ))


def run_script_on_node(node, script):
    from subprocess import check_call
    return check_call(['ssh', '-o StrictHostKeyChecking=no', node, script])


def run_script_on_n_nodes(n, script, log=False):
    import itertools as it
    for node in it.islice(nodes(), n):
        res = run_script_on_node(node, script)
        if log:
            print('{0}: {1}'.format(node, res))


if __name__ == '__main__':
    run_script_on_n_nodes(MAX_NODES, SCRIPT, log=True)
