#!/usr/bin/python

FILES = {
    '8': [
        #'/lhcb/LHCb/Collision12/MINIBIAS.DST/00041834/0000/00041834_00000113_1.minibias.dst',
        '/lhcb/LHCb/Collision12/MINIBIAS.DST/00041834/0000/00041834_00000303_1.minibias.dst',
        '/lhcb/LHCb/Collision12/MINIBIAS.DST/00041834/0000/00041834_00000329_1.minibias.dst',
        #'/lhcb/LHCb/Collision12/MINIBIAS.DST/00041834/0000/00041834_00000388_1.minibias.dst',
        #'/lhcb/LHCb/Collision12/MINIBIAS.DST/00041834/0000/00041834_00000407_1.minibias.dst',
        #'/lhcb/LHCb/Collision12/MINIBIAS.DST/00041834/0000/00041834_00000549_1.minibias.dst',
        #'/lhcb/LHCb/Collision12/MINIBIAS.DST/00041834/0000/00041834_00000717_1.minibias.dst',
        #'/lhcb/LHCb/Collision12/MINIBIAS.DST/00041834/0000/00041834_00000752_1.minibias.dst',
        #'/lhcb/LHCb/Collision12/MINIBIAS.DST/00041834/0000/00041834_00000787_1.minibias.dst',
        #'/lhcb/LHCb/Collision12/MINIBIAS.DST/00041834/0000/00041834_00000931_1.minibias.dst',
        #'/lhcb/LHCb/Collision12/MINIBIAS.DST/00041834/0000/00041834_00000940_1.minibias.dst',
        #'/lhcb/LHCb/Collision12/MINIBIAS.DST/00041834/0000/00041834_00000965_1.minibias.dst',
        #'/lhcb/LHCb/Collision12/MINIBIAS.DST/00041834/0000/00041834_00000986_1.minibias.dst'
    ],
    '7': [
        '/lhcb/LHCb/Collision11/MINIBIAS.DST/00041838/0000/00041838_00000332_1.minibias.dst',
        '/lhcb/LHCb/Collision11/MINIBIAS.DST/00041838/0000/00041838_00000532_1.minibias.dst'
    ]
}


def get_file(lfn, target, log=False):
    from subprocess import call
    call('dirac-dms-get-file --Directory {1} {0}'.format(lfn, target),
         shell=True)


def ensure_directory(dir):
    from os import (
        makedirs,
        path
    )
    if not path.isdir(dir):
        makedirs(dir)


if __name__ == '__main__':
    from os import path
    for tev, files in FILES.iteritems():
        target = path.join('files', tev)
        ensure_directory(target)
        for lfn in files:
            get_file(lfn, target, log=True)
