""" Run L0 App locally in order to extract performance information.
    Based on a script by Christian Linn.

    usage: gaudirun.py run_l0.py files.py
"""

from Gaudi.Configuration import *
from Gaudi.Configuration import ApplicationMgr
from Configurables import (
    AuditorSvc,
    EventSelector,
    GaudiSequencer,
    L0App,
    L0DUFromRawAlg,
    L0DUReportMonitor,
    LHCbTimingAuditor,
    LoKiSvc,
    LoKi__L0Filter
)
from Configurables import LHCb__MDFWriter as MDFWriter

L0App().TCK = '0x014E'
L0App().ReplaceL0Banks = True
L0App().EvtMax = -1
L0App().DataType = '2015'
L0App().Simulation = False
# 13 TeV
#L0App().DDDBtag = "dddb-20150724"
#L0App().CondDBtag = "cond-20150828"
# 8 TeV
#L0App().DDDBtag = 'head-20120413'
#L0App().CondDBtag = 'head-20120420'
# 7 TeV
L0App().DDDBtag = 'dddb-20130929'
L0App().CondDBtag = 'cond-20141107'

EventSelector().PrintFreq = 1000
l0du_alg = L0DUFromRawAlg()
l0du_mon = L0DUReportMonitor()

LoKiSvc().Welcome = False
filter_code = "|".join(["L0_CHANNEL('%s')" % chan
                        for chan in ['Muon', 'Muon,lowMult', 'DiMuon,lowMult',
                                     'Electron,lowMult', 'Photon,lowMult',
                                     'DiEM,lowMult', 'DiHadron,lowMult']])
l0_filter = LoKi__L0Filter('L0Filter', Code = filter_code)

#MDF Writer
filename = 'L0filtered_0x014E'
mdf_writer = MDFWriter('MDFWriter', Compress = 0, ChecksumType = 1, GenerateMD5 = True,
                       Connection = 'file://%s.mdf' % filename)

# Sequence
writer_seq = GaudiSequencer('WriterSeq')
writer_seq.Members = [l0du_alg, l0du_mon, l0_filter, mdf_writer]
ApplicationMgr().OutStream = [writer_seq]

# Timing table to make sure things work as intended
ApplicationMgr().AuditAlgorithms = 1
if 'AuditorSvc' not in ApplicationMgr().ExtSvc:
    ApplicationMgr().ExtSvc.append('AuditorSvc')
AuditorSvc().Auditors.append(LHCbTimingAuditor(Enable = True))
