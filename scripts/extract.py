#!/usr/bin/env python


def clopper_pearson(k, n, alpha=0.32):
    from scipy import stats
    lo = stats.beta.ppf(alpha / 2, k, n - k + 1)
    hi = stats.beta.ppf(1 - alpha / 2, k + 1, n - k)
    return lo, hi


def efficiency(k, n, *args, **kwargs):
    eff = 1.0 * k / n
    err = clopper_pearson(k, n, *args, **kwargs)
    err = (eff - err[0], err[1] - eff)
    return eff, err


def format_efficiency(e):
    eff, err = e
    str_eff = '{:.2f}'.format(eff * 100)
    str_lo = '{:.2f}'.format(err[0] * 100)
    str_hi = '{:.2f}'.format(err[1] * 100)
    if str_lo == str_hi:
        return r'({} \pm {})\,\%'.format(str_eff, str_lo)
    return r'({}^{{+{}}}_{{-{}}})\,\%'.format(str_eff, str_hi, str_lo)


def extract_muon_retention(filename):
    import re
    pat_evt = re.compile(r'L0 Performance on (\d+) events')
    pat_mu = re.compile(r'SubTrigger\s+:\s+Physics\|L0Muon\s+:\s+(\d+) events')
    with open(filename) as logfile:
        data = logfile.read()
        m_evt = pat_evt.search(data)
        m_mu = pat_mu.search(data)
    if not m_evt or not m_mu:
        return None
    return int(m_mu.group(1)), int(m_evt.group(1))


if __name__ == '__main__':
    import sys
    fname = 'log.log'
    if len(sys.argv) > 1:
        fname = sys.argv[1]
    print(format_efficiency(efficiency(*extract_muon_retention(fname))))
