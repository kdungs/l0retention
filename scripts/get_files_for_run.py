#!/usr/bin/env python


BASE = '/localdisk/Alignment/BWDivision'
LOC = '/home/kdungs/data/l0retention/files/13'
RUN = 166902


def files():
    from os import path
    from glob import glob
    globstr = path.join(BASE, 'Run_{0:07}_*.mdf'.format(RUN))
    return glob(globstr)


def copy(files):
    from shutil import copy
    return [copy(f, LOC) for f in files]


if __name__ == '__main__':
    copy(files())
